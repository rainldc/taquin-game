function Agent(grid, id) {
  this.grid = grid;
  this.id = id;
  this.shortestWay = [];
  this.letterBox = new LinkedList();
}

/**
 * Returns the agent's current state
 * @returns {row, column}
 */
Agent.prototype.getCurrentState = function() {
  return this.grid.getStateOf(this);
};

/**
 * Returns the agent's goal state
 * @returns {row, column}
 */
Agent.prototype.getTerminalState = function() {
  return this.grid.getTerminalStateOf(this);
};

/**
 * Returns all agents on the grid
 * @returns Agent[]
 */
Agent.prototype.getAgents = function() {
  return this.grid.agents;
};

Agent.prototype.determineShortestWay = function(that) {
  this.easystar = new EasyStar.js();
  this.easystar.setGrid(this.grid.getGrid());
  this.easystar.setAcceptableTiles([0]);

  this.findAgentPath(this.getCurrentState(), this.getTerminalState());
};

Agent.prototype.sendMoveRequestTo = function(agentId) {
  let agentsList = this.getAgents();
  let agentLetterbox = agentsList[agentId].letterBox;
  if (!agentLetterbox.contains(this.id)) {
    if (agentLetterbox.getHead() === null) {
      agentLetterbox.insertAtFirst(this.id);
    } else {
      agentLetterbox.insertNodeAt(
        this.id,
        agentLetterbox.getLinkedListLength() + 1
      );
    }
  }
};

Agent.prototype.sendMoveRequestToNeighbor = function(coordinates) {
  if (coordinates !== undefined) {
    let index = this.grid.agentPositions.findIndex(
      agent => agent.row === coordinates.x && agent.column === coordinates.y
    );
    this.sendMoveRequestTo(index);
  } else {
    let agentEnvironment = this.getEnvironment();
    let agentsNeighbors = agentEnvironment.filter(
      location => location.agentId > -1
    );
    agentsNeighbors.forEach(location => {
      this.sendMoveRequestTo(location.agentId);
    });
  }
};

Agent.prototype.getEnvironment = function() {
  let { row: sourceRow, column: sourceColumn } = this.getCurrentState();

  return [
    {
      position: "NORTH",
      agentId: this.grid.agentPositions.findIndex(
        agentPosition =>
          agentPosition.row === sourceRow - 1 &&
          agentPosition.column === sourceColumn
      )
    },
    {
      position: "EAST",
      agentId: this.grid.agentPositions.findIndex(
        agentPosition =>
          agentPosition.row === sourceRow &&
          agentPosition.column === sourceColumn + 1
      )
    },
    {
      position: "WEST",
      agentId: this.grid.agentPositions.findIndex(
        agentPosition =>
          agentPosition.row === sourceRow &&
          agentPosition.column === sourceColumn - 1
      )
    },
    {
      position: "SOUTH",
      agentId: this.grid.agentPositions.findIndex(
        agentPosition =>
          agentPosition.row === sourceRow + 1 &&
          agentPosition.column === sourceColumn
      )
    }
  ];
};
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
Agent.prototype.findAgentPath = function(
  { row, column },
  { row: terminalRow, column: terminalColumn }
) {
  let that = this;
  let instanceID = this.easystar.findPath(
    row,
    column,
    terminalRow,
    terminalColumn,
    async path => {
      let agentEnvironment = this.getEnvironment();
      if (this.letterBox.getLinkedListLength() !== 0) {
        let freeSquares = agentEnvironment.filter(agent => agent.agentId < 0);
        if (freeSquares.length > 0) {
          let randomIndex = getRandomNumber(freeSquares.length);
          let position = freeSquares[randomIndex].position;
          let coordinates = this.grid.getCoordinates(
            position,
            this.grid.agentPositions[this.id]
          );

          if (coordinates !== undefined) {
            this.moveTo(coordinates);
            await sleep(10);
          }
          this.letterBox.clearAllNodes();
        } else {
          let agentEnvironment = this.getEnvironment();
          let agentsNeighbors = agentEnvironment.filter(
            location => location.agentId > -1
          );
          let randomIndex = getRandomNumber(agentsNeighbors.length);
          this.sendMoveRequestTo(agentsNeighbors[randomIndex].agentId);
        }
      } else if (path !== null) {
        if (path.length > 1) {
          path.splice(0, 1);

          if (that.grid.stateIsOccupied(path[0])) {
            let agentEnvironment = this.getEnvironment();
            let agentsNeighbors = agentEnvironment.filter(
              location => location.agentId > -1
            );
            let randomIndex = getRandomNumber(agentsNeighbors.length);
            this.sendMoveRequestTo(agentsNeighbors[randomIndex].agentId);
          } else {
            that.moveTo(path[0]);
          }
        }
      }
    }
  );

  this.easystar.calculate();
};

Agent.prototype.run = async function() {
  let that = this;
  this.intervalID = setInterval(() => {
    this.determineShortestWay(that);
  }, 1);
};

Agent.prototype.moveTo = function({ x: row, y: column }) {
  let { row: sourceRow, column: sourceColumn } = this.getCurrentState();
  let sourceTarget = document.getElementById(sourceRow + "-" + sourceColumn);
  sourceTarget.getElementsByClassName("agent")[0].innerHTML = "";

  let squareTarget = document.getElementById(row + "-" + column);
  squareTarget.getElementsByClassName(
    "agent"
  )[0].innerHTML = this.id.toString();

  this.grid.agentPositions[this.id] = { row: row, column: column };
  //console.log(this.grid.isResolved());
};
