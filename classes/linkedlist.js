class LinkedList {
    constructor() {
        this.head = null;
    }

    /**
     *
     * @param data donnée du noeud
     * insère le noeud en tête de liste
     */
    insertAtFirst(data) {
        let newNode = new LinkedListNode(data);
        newNode.next = this.head;
        this.head = newNode;
        return this.head;
    }

    insertAtLastPosition(data) {
        let newNode = new LinkedListNode(data);

        if (!this.head) {
            this.head = newNode;
            return this.head;
        }

        let potentialTail = this.head;
        while (potentialTail.next !== null) {
            potentialTail = potentialTail.next;
        }
        potentialTail.next = newNode;

        return this.head;
    }

    /**
     *
     * @param index
     * @returns {LinkedListNode}
     */
    getNodeAt(index) {
        let i = 0;
        let node = this.head;
        while (node) {
            if (i === index) {
                return node;
            }
            i++;
            node = node.next;
        }
        return null;
    }

    insertNodeAt(data, index) {
        if (!this.head) {
            this.head = new LinkedListNode(data);
            return;
        }

        if (index === 0) {
            this.head = new LinkedListNode(data, this.head);
            return;
        }

        const previousNode = this.getNodeAt(index - 1);
        let nodeToInsert = new LinkedListNode(data);
        nodeToInsert.next = previousNode.next;
        previousNode.next = nodeToInsert;

        return this.head;
    }

    deleteHeadNode() {
        if (!this.head) {
            return;
        }
        this.head = this.head.next;
        return this.head;
    }

    clearAllNodes() {
        if (!this.head || !this.head.next) {
            this.head = null;
        }

        while (this.head !== null) {
            this.head = this.head.next;
            this.deleteHeadNode();
        }
    }

    deleteLastNode() {
        if (!this.head) {
            return null;
        }

        if (!this.head.next) {
            this.head = null;
            return;
        }

        let previousNode = this.head;
        let tailNode = this.head.next;
        while (tailNode.next !== null) {
            previousNode = tailNode;
            tailNode = tailNode.next;
        }
        previousNode.next = null;
        return this.head;
    }

    getLinkedListLength() {
        let i = 0;
        let currentNode = this.head;

        if (currentNode === null) {
            return 0;
        }

        while (currentNode.next !== null) {
            i++;
            currentNode = currentNode.next;
        }
        return i;
    }

    deleteNodeAt(index) {
        if (!this.head) {
            return null;
        }

        if (index === 0) {
            this.head = this.head.next;
            return;
        }

        const previousNode = this.getNodeAt(index - 1);

        if (!previousNode || !previousNode.next) {
            return;
        }

        previousNode.next = previousNode.next.next;
        return this.head;
    }

    getHead() {
        return this.head;
    }

    contains(value) {
        let currentNode = this.head;

        if (currentNode === null) {
            return 0;
        }

        while (currentNode.next !== null) {
            if (currentNode.data === value) {
                return true;
            }
            currentNode = currentNode.next;
        }
        return false;
    }
}