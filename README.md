# TP Taquin SMA

## Introduction
Ce rapport présente le taquin que nous avons réalisé avec le langage JavaScript. L'objectif du jeu du taquin est, à partir d'un modèle de puzzle cible, de retrouver ce modèle à partir d'un mélange aléatoire des objets à replacer. Nous voulons automatiser la logique de ce jeu à l'aide d'un système multi-agents.

L'application est disponible à ce lien: https://leadrain.gitlab.io/taquin-game/

## Design

Dans cette application nous avons 3 boutons : 
- Générer puzzle aléatoire
- Lancer la partie simulée
- Stop (Faire pause)

Il faut d'abord générer le puzzle aléatoire puis lancer la partie simulée.
Sur la gauche, nous avons la grille initiale du taquin. Au lancement de la partie, nos agents vont communiquer entre eux pour accéder au fur et à mesure aux cases qui les mèneront vers leur case objectif que l'on voit ci-dessous à droite.

![](https://i.imgur.com/1SCkoJS.png)


## Explication du code
Pour mettre en place notre taquin, nous avions besoin de définir la structure de notre application : 
- Les agents : ils sont représentés par des numéros. Ils peuvent se déplacer d'une case à une autre selon les mouvements cardinaux. Ils possèdent une boîte aux lettres sous la forme d'une liste chaînée permettant de communiquer avec les autres agents du système.
- L'environnement : il s'agit d'une grille 5x5. Dans cette grille, on peut accéder à :
    - sa taille
    - la position courante des agents
    - la position finale à laquelle les agents du système doivent se trouver

Pour rendre nos taquins résolvables, nous avons supposé que les agents du jeu étaient tous coopératifs. Ce choix se justifie par le fait que si nous rendions nos agents individualistes, nos chances de résoudre un taquin donné seraient très faibles.
Nous avons mis en place l'algorithme A* pour trouver le meilleur chemin menant à l'objectif de l'agent considéré. Si un obstacle est détecté, on poste un message aux agents voisins pour leur demander de bouger vers une autre case libre si possible. 

//TODO: ajouter le pseudo code

## Résultats

Nous arrivons à résoudre entièrement tous les taquins remplis de 0 à 23 cases sur 25, soit 99% de remplissage. Nous arrivons également à résoudre des taquins comportant 24 cases remplies sur 25. Cependant, il faut préciser qu'il existe des configurations de taquin à 24 cases remplies sur 25, qui ne peuvent pas être résolus à cause de la parité notamment (cas de Sam Loyd avec inversion de 2 cases adjacentes par exemple).

## Déploiment 

### Gitlab Pages

Pour déployer notre application, nous avons utilisé gitlab Pages. C'est une fonctionnalité qui permet de publier des sites web statiques directement à partir d'un référentiel dans GitLab. L'utilisation d'une CI/CD à travers le fichier .gtilab.yml nous permet de mettre à jours notre site à la suite d'une merge request vers master. 

Les pages GitLab 

### CI/CD

Dans le domaine du génie logiciel, le terme CI/CD ou CICD désigne généralement les pratiques combinées d'intégration continue et de livraison ou de déploiement continus.

Dans le contexte de la communication d'entreprise, le terme CI/CD peut également faire référence au processus global de l'identité et de la conception d'entreprise.

### Docker

L'application est dockerisable. On grace au dockerfile la lancer sur un serveur web et y accéder depuis un navigateur.

Docker est un ensemble de produits de type "platform as a service" (PaaS) qui utilisent la virtualisation au niveau du système d'exploitation pour fournir des logiciels dans des paquets appelés "conteneurs". Les conteneurs sont isolés les uns des autres et regroupent leurs propres logiciels, bibliothèques et fichiers de configuration ; ils peuvent communiquer entre eux par des canaux bien définis. Tous les conteneurs sont gérés par un seul noyau de système d'exploitation et sont donc plus légers que les machines virtuelles. 

Le service comporte des volets gratuits et des volets payants. Le logiciel qui héberge les conteneurs est appelé Docker Engine. Il a été lancé en 2013 et est développé par Docker, Inc.

Voici le dockerfile : 

```dockerfile=
FROM nginx:stable-alpine
COPY . /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
```

Les commandes à lancer : 

```bash
cd /taquin-game
docker build -t taquin .
docker run -it -p 8080:80 --rm --name taquin -d taquin 
```