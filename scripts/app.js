const gridSize = 5;
const numberOfAgents = 10;

let app = {
  run: function() {
    document
      .getElementById("generateButton")
      .addEventListener("click", app.generatePuzzle);
    document.getElementById("runButton").addEventListener("click", app.runGame);
    document
      .getElementById("stopButton")
      .addEventListener("click", app.stopAgents);
    console.log("[Taquin] App is running.");
  },

  runGame: function() {
    if (!app.grid) return;
    console.log("[Taquin] Run game.");
    app.agents.forEach(agent => agent.run().then());
  },

  stopAgents: function() {
    console.log("[Taquin] Agents stopped.");
    if (app.agents) {
      app.agents.forEach(agent => {
        if (agent.intervalID) {
          clearInterval(agent.intervalID);
          delete agent.intervalID;
        }
      });
    }
  },

  generatePuzzle: function() {
    app.stopAgents();

    //const numberOfAgents = document.getElementById("inputAgent").value;

    console.log("[Taquin] Generate grid.");
    app.generateGridLayout();

    console.log("[Taquin] Generate agents.");
    app.createAgents(numberOfAgents);
    app.initStates();
  },

  generateGridLayout: function() {
    //if (document.getElementById("inputGrid").value >= 24) const gridSize = 24;
    //else if (document.getElementById("inputGrid").value <= 1)
    //  const gridSize = 1;

    app.grid = new Grid(gridSize);
    let gridLayout = document.getElementById("grid");
    gridLayout.innerHTML = "";
    let squareLayout = document.createElement("div");
    squareLayout.classList.add("col");
    squareLayout.innerHTML = "<span class='agent'></span>";

    for (let i = 0; i < app.grid.matrixSize; i++) {
      for (let j = 0; j < app.grid.matrixSize; j++) {
        let newSquare = squareLayout.cloneNode(true);
        newSquare.id = i + "-" + j;
        gridLayout.appendChild(newSquare);
      }
    }

    let gridTargetLayout = document.getElementById("grid_target");
    gridTargetLayout.innerHTML = "";
    let squareTargetLayout = document.createElement("div");
    squareTargetLayout.classList.add("col");
    squareTargetLayout.innerHTML = "<span class='goal'>";
    for (let i = 0; i < app.grid.matrixSize; i++) {
      for (let j = 0; j < app.grid.matrixSize; j++) {
        let newSquare = squareTargetLayout.cloneNode(true);
        newSquare.id = i + "_" + j;
        gridTargetLayout.appendChild(newSquare);
      }
    }

    document.getElementById("taqH2").innerHTML = "Taquin";
    document.getElementById("objH2").innerHTML = "Objectif";
  },

  createAgents: function(number) {
    app.agents = [];
    for (let i = 0; i < number; i++) {
      app.agents[i] = new Agent(app.grid, i);
    }
  },

  initStates: function() {
    app.grid.placeAgents(app.agents);
    app.grid.agentPositions.forEach(({ row, column }, id) => {
      let squareTarget = document.getElementById(row + "-" + column);
      squareTarget.getElementsByClassName("agent")[0].innerHTML = id.toString();
    });
    app.grid.agentsGoals.forEach(({ row, column }, id) => {
      document
        .getElementById(row + "_" + column)
        .getElementsByClassName("goal")[0].innerHTML = id.toString();
    });
  }
};

document.addEventListener("DOMContentLoaded", app.run);
